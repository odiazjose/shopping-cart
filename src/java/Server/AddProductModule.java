package Server;

import Ortega.JDBConnector.JDBConnector;
import Ortega.JSONBuilder.JSONBuilder;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "AddProductModule", urlPatterns = {"/AddProductModule"})
public class AddProductModule extends HttpServlet {
    private JSONBuilder builder = new JSONBuilder();
    private JDBConnector conn;

    public AddProductModule() throws ClassNotFoundException, SQLException{
        try{
            conn = new JDBConnector("localhost", 5432, "postgres", "masterkey", "ShoppingCart");
        }catch(ClassNotFoundException e){
            System.err.printf("The connection with the database could not be established.", e.getMessage());
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Integer userID = Integer.parseInt(request.getSession(false).getAttribute("userID").toString());
        
        String name = request.getParameter("name");
        String description = request.getParameter("desc");
        Double cost = Double.parseDouble(request.getParameter("cost"));
        Integer count = Integer.parseInt(request.getParameter("count"));
        byte[] imageData = request.getParameter("image").getBytes();
        
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
            Date date = new Date();
                        
            conn.query("INSERT INTO product(name_pro, desc_pro, cost_pro, count_pro, image_pro) VALUES(?, ?, ?, ?, ?) RETURNING id_pro", name, description, cost, count, imageData);
            Integer productID = (Integer) conn.result[1][0];
            
            conn.query("INSERT INTO sale(id_user) VALUES(?) RETURNING id_sale", userID);
            Integer saleID = (Integer) conn.result[1][0];
            
            conn.statement("INSERT INTO sale_product(id_sale, id_pro, id_user, date_sale, count_sale) VALUES(?, ?, ?, ?, ?)", saleID, productID, userID, dateFormat.format(date), count);
            
            builder.add("productAdded", true);
        } catch (SQLException ex) {
            builder.add("productAdded", false);
            builder.add("message", "Ha ocurrido un error en la base de datos. Error: " + ex.getMessage());
        } finally {
            response.getWriter().print(builder.build());
            
            builder.clear();
        }
    }
}
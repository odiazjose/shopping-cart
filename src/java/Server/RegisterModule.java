package Server;

import Ortega.JDBConnector.JDBConnector;
import Ortega.JSONBuilder.JSONBuilder;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "RegisterModule", urlPatterns = {"/RegisterModule"})
public class RegisterModule extends HttpServlet {
    private JSONBuilder builder = new JSONBuilder();
    private JDBConnector conn;

    public RegisterModule() throws ClassNotFoundException, SQLException{
        try{
            conn = new JDBConnector("localhost", 5432, "postgres", "masterkey", "ShoppingCart");
        }catch(ClassNotFoundException e){
            System.err.printf("The connection with the database could not be established.", e.getMessage());
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        String lastName = request.getParameter("lastName");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        Double fund = Double.parseDouble(request.getParameter("fund"));
        
        try {
            conn.statement("INSERT INTO "
                    + "member(name_user, lastn_user, usern_user, pass_user, email_user, fund_user) "
                    + "VALUES(?, ?, ?, ?, ?, ?)", name, lastName, username, password, email, fund);
            
            builder.add("registered", true);
        } catch (SQLException ex) {
            builder.add("registered", false);
            
            if(ex.getSQLState().equals("23505")) builder.add("message", "Ese nombre de usuario ya se encuentra en uso.");
            else builder.add("message", "Ha ocurrido un error en la base de datos. Error: " + ex.getMessage());
        } finally {
            response.getWriter().print(builder.build());
            
            builder.clear();
        }
    }
}
package Server;

import Ortega.JDBConnector.JDBConnector;
import Ortega.JSONBuilder.JSONBuilder;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Purchases", urlPatterns = {"/Purchases"})
public class Purchases extends HttpServlet {
    private JSONBuilder builder = new JSONBuilder();
    private JDBConnector conn;

    public Purchases() throws ClassNotFoundException, SQLException{
        try{
            conn = new JDBConnector("localhost", 5432, "postgres", "masterkey", "ShoppingCart");
        }catch(ClassNotFoundException e){
            System.err.printf("The connection with the database could not be established.", e.getMessage());
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Integer userID = Integer.parseInt(request.getSession(false).getAttribute("userID").toString());
        
        try {
            conn.query("SELECT "
                    + "id_pro, cost_pro, count_pro, SUM(cart.count) AS count_pro_cart "
                    + "FROM member, product RIGHT JOIN "
                    + "(SELECT unnest(cart_user[1:array_length(cart_user, 1)][1:1]) AS id, "
                    + "unnest(cart_user[1:array_length(cart_user, 1)][2:2]) AS count "
                    + "FROM member WHERE id_user = ?) AS cart "
                    + "ON cart.id = id_pro "
                    + "WHERE id_user = ? "
                    + "GROUP BY id_pro",
                    userID, userID);
            
            Integer proAmmount = conn.result.length - 1;
            
            Float userFunds;
            
            Integer[] productID;
            Float totalPrice = 0f;
            Float[] productPrice;
            Integer[] productCount;
            Long[] count_pro_cart;
            
            Float[] vendorPayment;
            
            Boolean checkProductAmmount = true;
            
            if(proAmmount > 0){
                productID = new Integer[proAmmount];
                productPrice = new Float[proAmmount];
                productCount = new Integer[proAmmount];
                count_pro_cart = new Long[proAmmount];
                vendorPayment = new Float[proAmmount];
                
                for(Integer i = 1; i < conn.result.length; i++){
                    productID[i-1] = (Integer) conn.result[i][0];
                    productPrice[i-1] = (Float) conn.result[i][1];
                    productCount[i-1] = (Integer) conn.result[i][2];
                    count_pro_cart[i-1] =  (Long) conn.result[i][3];
                    
                    vendorPayment[i-1] = productPrice[i-1]*count_pro_cart[i-1];
                    
                    totalPrice += vendorPayment[i-1];
                }
                
                conn.query("SELECT fund_user FROM member WHERE id_user = ?", userID);
                
                userFunds = (Float) conn.result[1][0];
                
                if(userFunds > totalPrice){
                    for(Integer i = 0; i < proAmmount; i++){
                        if(productCount[i] < count_pro_cart[i]){
                            checkProductAmmount = false;
                            
                            builder.add("success", false);
                            builder.add("message", "No hay sufientes ejemplares de uno de los artículos a comprar.");
                            builder.add("itemID", productID[i]);
                            break;
                        }
                    }
                    
                    if(checkProductAmmount){
                        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                        Date date = new Date();
                        
                        conn.query("INSERT INTO purchase(id_user) VALUES(?) RETURNING id_pur", userID);
                        Integer purchaseID = (Integer) conn.result[1][0];
                        
                        conn.statement("UPDATE member SET fund_user = fund_user - ?, cart_user = NULL WHERE id_user = ?", totalPrice, userID);
                        
                        for(Integer i = 0; i < proAmmount; i++){
                            conn.statement("UPDATE product SET count_pro = count_pro - ? WHERE id_pro = ?", count_pro_cart[i], productID[i]);
                            conn.statement("UPDATE member SET fund_user = fund_user + ? FROM sale_product WHERE member.id_user = sale_product.id_user AND sale_product.id_pro = ?", vendorPayment[i], productID[i]);
                            conn.statement("INSERT INTO product_user(id_pro, id_user) VALUES(?, ?)", productID[i], userID);
                            conn.statement("INSERT INTO purchase_product(id_pur, id_pro, id_user, date_pur, count_pur) VALUES (?, ?, ?, ?, ?)", purchaseID, productID[i], userID, dateFormat.format(date), count_pro_cart[i]);         
                        }
                        
                        builder.add("success", true);
                    }
                }else{
                    builder.add("success", false);
                    builder.add("message", "No tiene suficiente dinero para realizar la compra. Maldito pobre");
                }
            }
        } catch (SQLException ex) {
            builder.add("message", ex.getMessage());
        } finally {
            response.getWriter().print(builder.build());
            
            builder.clear();
        }
    }
}
package Server;

import Ortega.JDBConnector.JDBConnector;
import Ortega.JSONBuilder.JSONBuilder;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ViewProductModule", urlPatterns = {"/ViewProductModule"})
public class ViewProductModule extends HttpServlet {
    private JSONBuilder builder = new JSONBuilder();
    private JDBConnector conn;

    public ViewProductModule() throws ClassNotFoundException, SQLException{
        try{
            conn = new JDBConnector("localhost", 5432, "postgres", "masterkey", "ShoppingCart");
        }catch(ClassNotFoundException e){
            System.err.printf("The connection with the database could not be established.", e.getMessage());
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Integer pageNumber = Integer.parseInt(request.getParameter("itemsPage"));
        Integer itemsAmount = Integer.parseInt(request.getParameter("itemsAmount"));
        
        Integer inferiorLimit = pageNumber*itemsAmount;
        Integer superiorLimit = inferiorLimit + itemsAmount;
        
        try {
            conn.query("SELECT "
                    + "product.id_pro, name_pro, cost_pro, count_pro, desc_pro, image_pro "
                    + "FROM product, sale_product WHERE product.id_pro > ? AND product.id_pro <= ? "
                    + "AND product.id_pro = sale_product.id_pro AND sale_product.id_user != ?", 
                    inferiorLimit, superiorLimit, request.getSession().getAttribute("userID"));
            
            if(conn.result.length > 1){
                for(Integer i = 1; i < conn.result.length; i++)
                    conn.result[i][5] = new String((byte[]) conn.result[i][5]);
                
                builder.addMatrixAsJSONArray("items", conn.result);
            }else{
                builder.add("success", false);
                builder.add("message", "No hay otros productos disponibles.");
            }
            
        } catch (SQLException ex) {
            builder.add("success", false);
            builder.add("message", "Ha ocurrido un error en la base de datos. Error: " + ex.getMessage());
        } finally {
            response.getWriter().print(builder.build());
            
            builder.clear();
        }
    }
}
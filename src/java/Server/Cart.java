package Server;

import Ortega.JDBConnector.JDBConnector;
import Ortega.JSONBuilder.JSONBuilder;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "Cart", urlPatterns = {"/Cart"})
public class Cart extends HttpServlet {
    private JSONBuilder builder = new JSONBuilder();
    private JDBConnector conn;
    
    public Cart() throws ClassNotFoundException, SQLException{
        try{
            conn = new JDBConnector("localhost", 5432, "postgres", "masterkey", "ShoppingCart");
        }catch(ClassNotFoundException e){
            System.err.printf("The connection with the database could not be established.", e.getMessage());
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        
        try {
            conn.query("SELECT "
                    + "id_pro, name_pro, cost_pro, desc_pro, image_pro, SUM(cart.count) AS count_pro_cart "
                    + "FROM member, product RIGHT JOIN "
                        + "(SELECT unnest(cart_user[1:array_length(cart_user, 1)][1:1]) AS id, "
                        + "unnest(cart_user[1:array_length(cart_user, 1)][2:2]) AS count "
                        + "FROM member WHERE id_user = ?) AS cart "
                    + "ON cart.id = id_pro "
                    + "WHERE id_user = ? "
                    + "GROUP BY id_pro",
                    session.getAttribute("userID"), session.getAttribute("userID"));
            
            if(conn.result.length > 1){
                for(Integer i = 1; i < conn.result.length; i++)
                    conn.result[i][4] = new String((byte[]) conn.result[i][4]);
                
                builder.add("success", true);
                builder.addMatrixAsJSONArray("products", conn.result);
            }else{
                builder.add("success", false);
                builder.add("message", "No hay productos en el carrito.");
            }
        } catch (SQLException ex) {
            builder.add("success", false);
            builder.add("message", ex.getMessage());
        } finally {
            response.getWriter().print(builder.build());
            
            builder.clear();
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        
        Integer productID = Integer.parseInt(request.getParameter("product"));
        Integer productQuantity = Integer.parseInt(request.getParameter("count"));
        
        try {
            conn.statement(
                    "UPDATE member SET cart_user = cart_user || ARRAY[[?, ?]] WHERE id_user = ?", 
                    productID, productQuantity, session.getAttribute("userID"));
            
            builder.add("added", true);
        } catch(SQLException ex){
            builder.add("added", false);
            builder.add("message", ex.getMessage());
        } finally {
            response.getWriter().print(builder.build());
            
            builder.clear();
        }
    }
}
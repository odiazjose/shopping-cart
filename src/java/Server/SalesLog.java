package Server;

import Ortega.JDBConnector.JDBConnector;
import Ortega.JSONBuilder.JSONBuilder;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "SalesLog", urlPatterns = {"/SalesLog"})
public class SalesLog extends HttpServlet {
    private JSONBuilder builder = new JSONBuilder();
    private JDBConnector conn;

    public SalesLog() throws ClassNotFoundException, SQLException{
        try{
            conn = new JDBConnector("localhost", 5432, "postgres", "masterkey", "ShoppingCart");
        }catch(ClassNotFoundException e){
            System.err.printf("The connection with the database could not be established.", e.getMessage());
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            conn.query("SELECT sale.id_sale, id_pro, date_sale, count_sale "
                    + "FROM sale RIGHT JOIN sale_product "
                    + "ON sale.id_sale = sale_product.id_sale "
                    + "WHERE sale.id_user = ?", 
                    request.getSession(false).getAttribute("userID"));
            
            if(conn.result.length > 1){
                builder.add("success", true);
                builder.addMatrixAsJSONArray("log", conn.result);
            }else{
                builder.add("success", false);
                builder.add("message", "Usted no ha colocado productos a la venta.");
            }
        } catch (SQLException ex) {
            builder.add("success", false);
            builder.add("message", "Un error ha ocurrido en la base de datos. " + ex.getMessage());
        } finally {
            response.getWriter().printf(builder.build());
            
            builder.clear();
        }
    }
}
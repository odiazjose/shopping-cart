package Server;

import Ortega.JSONBuilder.JSONBuilder;
import Ortega.JDBConnector.JDBConnector;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "SessionModule", urlPatterns = {"/SessionModule"})
public class SessionModule extends HttpServlet {
    private JSONBuilder builder = new JSONBuilder();
    private JDBConnector conn;

    public SessionModule() throws ClassNotFoundException, SQLException{
        try{
            conn = new JDBConnector("localhost", 5432, "postgres", "masterkey", "ShoppingCart");
        }catch(ClassNotFoundException e){
            System.err.printf("The connection with the database could not be established.", e.getMessage());
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if(session != null) session.invalidate();
        
        response.getWriter().print("{}");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userName = request.getParameter("username");
        String password = request.getParameter("password");
        
        try {
            conn.query("SELECT id_user FROM member WHERE usern_user = ? AND pass_user = ?", userName, password);
            
            if(conn.result.length > 1){
                builder.add("login", true);
                
                HttpSession session = request.getSession(true);
                session.setAttribute("userID", conn.result[1][0]);
            }else{
                builder.add("login", false);
                builder.add("message", "Su nombre de usuario o contraseña no coinciden. Verifique sus credenciales e intentelo de nuevo.");
            }
        } catch (SQLException ex) {
            builder.add("login", false);
            builder.add("message", "Ha ocurrido un error en la base de datos. Error: " + ex.getMessage());
        } finally {
            response.getWriter().print(builder.build());
            
            builder.clear();
        }
    }
}
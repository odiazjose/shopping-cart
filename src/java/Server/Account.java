package Server;

import Ortega.JDBConnector.JDBConnector;
import Ortega.JSONBuilder.JSONBuilder;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Account", urlPatterns = {"/Account"})
public class Account extends HttpServlet {
    private JSONBuilder builder = new JSONBuilder();
    private JDBConnector conn;

    public Account() throws ClassNotFoundException, SQLException{
        try{
            conn = new JDBConnector("localhost", 5432, "postgres", "masterkey", "ShoppingCart");
        }catch(ClassNotFoundException e){
            System.err.printf("The connection with the database could not be established.", e.getMessage());
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            conn.query("SELECT "
                    + "name_user, lastn_user, usern_user, email_user, fund_user "
                    + "FROM member WHERE id_user = ?", 
                    request.getSession(false).getAttribute("userID"));
            
            builder.add("success", true);
            builder.add("name", (String) conn.result[1][0]);
            builder.add("lastName", (String) conn.result[1][1]);
            builder.add("userName", (String) conn.result[1][2]);
            builder.add("email", (String) conn.result[1][3]);
            builder.add("funds", (float) conn.result[1][4]);
        } catch (SQLException ex) {
            builder.add("success", false);
            builder.add("message", "Un error ha ocurrido en la base de datos. " + ex.getMessage());
        } finally {
            response.getWriter().print(builder.build());
            
            builder.clear();
        }
    }
}
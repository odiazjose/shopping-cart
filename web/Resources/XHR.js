XHR = function(){
    var xhr = new XMLHttpRequest();

    this.get = function(url, asynchronousMode, callback){
        xhr.open("GET", url, asynchronousMode);

        xhr.onreadystatechange = function(){
                if(xhr.status === 200 && xhr.readyState === 4){
                    var json = JSON.parse(xhr.responseText);
                    
                    callback(json);
                }
        };

        xhr.send();
    };

    this.post = function(url, asynchronousMode, callback, json){
        var parameters = "";
        var firstParameter = true;

        for(var property in json){
            if(firstParameter === false) parameters += "&";
            else firstParameter = false;
            
            parameters += this.getProperty(json, property);
            
        }
        
        xhr.open("POST", url, asynchronousMode);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        xhr.onreadystatechange = function(){
            if(xhr.status === 200 && xhr.readyState === 4){
                var jsonResponse = JSON.parse(xhr.responseText);
                
                callback(jsonResponse);
            }
        };
        
        xhr.send(parameters);
    };

    this.getProperty = function(object, property){
        switch(typeof(object[property])){
            case "string":
            case "number":
            case "boolean":
                return property + '=' + object[property];
                break;
            case "object":
                if(Array.isArray(object[property])){
                    var arrayData = '';
                    
                    for(var i = 0; i < object[property].length; i++){
                        arrayData += property + '=' + object[property][i];
                        
                        if(i + 1 !== object[property].length) arrayData += '&';
                    }
                    
                    return arrayData;
                }else{
                    return property + '=' + JSON.stringify(object[property]);
                }
                
                break;
        }
    };
};
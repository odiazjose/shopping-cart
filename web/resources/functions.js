/*
**********************************************
* functions to show and hide both header menus: login and account.
**********************************************
 */
function showLogin(){
    document.getElementById("loginForm").style.display = "block";
    document.getElementById("loginForm").style.zIndex = 100;
}

function hideLogin(){
    document.getElementById("loginForm").style.display = "none";
}

function showAccForm(){
    document.getElementById("accountForm").style.display = "block";
    document.getElementById("accountForm").style.zIndex = 100;
}

function hideAccForm(){
    document.getElementById("accountForm").style.display = "none";
}

/*
 **********************************************
 * login function, takes care of reading login input and processing login to system.
 **********************************************
 */
function login(){
    var user = document.getElementById("user").value;
    var pass = document.getElementById("pass").value;

    if((user == "") || (pass == "")){
        alert("Por favor, complete el formulario de login");
        return;
    }

    var userInfo = {
    	username: user,
        password: pass
    };
    
    xhr.post({
    	"contentType": "application/x-www-form-urlencoded",
    	"url": "./SessionModule",
    	"params": userInfo,
    	"success": function(res){
    		var response = JSON.parse(res);
    		if(response.login == true){
    			document.getElementById("user").value = "";
    		    document.getElementById("pass").value = "";

    		    localStorage.setItem("username", user);
    		    localStorage.setItem("logged", true);

    		    showHeader(true);
    		    window.location.reload(true);
    		}else{
    			alert("Error: " + response.message);
    		}
    	}
    });
    
    document.getElementById("user").value = "";
    document.getElementById("pass").value = "";
}

/*
 **********************************************
 * logs the fuck out
 **********************************************
 */
function logout(){
	alert("Cerrar sesión? :(", function(){
		showHeader(false);

		localStorage.setItem("username", "");
		localStorage.setItem("logged", false);
		
		window.location = "index.html"
	}, "Cancelar");
}

/*
 **********************************************
 * opens purchase.html, which contains the indexed list of all items the user can buy
 **********************************************
 */
function goShopping(){
    window.location="shopping.html";
}


/*
 **********************************************
 * opens sell.html, the interface for registering a new item for sale
 **********************************************
 */
function goSell(){
	if(localStorage.getItem("logged")==="true"){
		window.location="sell.html"
	}else{
		alert("Para vender un artículo debe ingresar al sistema.");
	}
}

/*
 **********************************************
 * opens register.html, which contains the form for registering new users
 **********************************************
 */

function goRegister(){
    window.location="reguser.html";
}

/*
 **********************************************
 * opens account.html, which contains account information such as
 * personal, sales and purchases data.
 **********************************************
 */
function goAccount(){
    window.location="account.html";
}

/*
 **********************************************
 * opens cart.html, which contains the shopping cart
 **********************************************
 */
function goCart(){
    window.location="cart.html";
}

/*
 **********************************************
 * yankees go home
 **********************************************
 */
function goHome(){
    window.location="index.html";
}

/*
 **********************************************
 * opens selllog.html to show sell log
 **********************************************
 */
function goSalesLog(){
    window.location="saleslog.html";
}

/*
 **********************************************
 * opens purchlog.html to show purchase log
 **********************************************
 */
function goPurchLog(){
    window.location="purchlog.html";
}

function showHeader(option){
    var afterLogin = document.getElementById("afterLogin");
    var beforeLogin = document.getElementById("beforeLogin");
    var username_label = document.getElementById("username-label");

    if(option){
        afterLogin.style.display = "block";
        beforeLogin.style.display = "none";
        username_label.innerHTML = localStorage.getItem("username");
    }else{
        afterLogin.style.display = "none";
        beforeLogin.style.display = "block";
        username_label.innerHTML = "";
    }
}

/*
 **********************************************
 * overrides alert() function to show a styled pop-up div with the required message
 **********************************************
 */
function alert(message, callback, cancel){
    if(document.getElementsByClassName("alert").length == 0) {

        var alertWindow = document.createElement("div");
        alertWindow.className = "alert";

        var icon = document.createElement("img");
        icon.src = "images/alert.png";
        alertWindow.appendChild(icon);

        var msg = document.createElement("p");
        msg.innerHTML = message;
        alertWindow.appendChild(msg);

        var accept = document.createElement("button");
        accept.innerHTML = "Aceptar";
        
        var cancel = document.createElement("button");
        cancel.innerHTML = "Cancelar";
        
        if(arguments.length == 1){
	        accept.addEventListener("click", function () {
	            document.body.removeChild(alertWindow);
	        }, false);
        }else{
        	accept.addEventListener("click", function () {
	            document.body.removeChild(alertWindow);
	        	callback();
	        }, false);
        	
        	cancel.addEventListener("click", function(){
        		document.body.removeChild(alertWindow);
        	}, false);
        }

        alertWindow.appendChild(accept);
        if(arguments.length == 3){
        	alertWindow.appendChild(cancel);
        }
        
        document.body.appendChild(alertWindow);

    }

}
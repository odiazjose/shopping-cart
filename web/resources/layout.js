/*
 **********************************************
 * writes the layout of the header and sidebar which is part of every page in the site.
 * must be called before anything else is loaded.
 **********************************************
 */

function layout(){
    document.write('' +
        '<header id="header" class="header">' +

            '<div class="title" onclick="goHome();">Orinoco -- Compra y Vende lo que Sea</div>' +

            '<div id="beforeLogin" class="beforeLogin">' +
                '<div id="showLogButton" class="showLogButton">&iexcl;Ingresa!</div>' +
                '<form id="loginForm" class="loginForm"> <br/>' +
                    '<label for="user">Usuario</label><br/>' +
                    '<input type="text" id="user" maxlength="12"/> <br/>' +
                    '<label for="pass">Contrase&ntilde;a</label><br/> ' +
                    '<input type="password" id="pass" />' +
                    '<p id="logButton" class="hButton" onclick="login()">Ingresar</p> <hr/>' +
                    '<p id="regButton" class="hButton" onclick="goRegister()">&iexcl;O Reg&iacute;strate ahora!</p>' +
                '</form>' +
            '</div>' +

            '<div id="afterLogin" class="afterLogin">' +
                '<div id="cartButton" class="cartButton" onclick="goCart()">Carrito' +
                    '<img src="images/cart.png" class="icon"/>' +
                '</div>' +
                '<div id="accountButton" class="accountButton"><p id="username-label"></p>' +
                    '<img src="images/account.png" class="icon"/>' +
                '</div>' +
                '<form id="accountForm" class="accountForm">' +
                    '<p id="accButton" class="hButton" onclick="goAccount()">Ver Cuenta</p><hr/>' +
                    '<p id="outButton" class="hButton" onclick="logout()">Salir</p>' +
                '</form>' +
            '</div>' +

        '</header>' +

        '<section id="sidebar" class="sidebar">' +
        
        	'<div id="corner" class="corner" onclick="goHome()">Orinoco</div>' + 

            '<div id="buyButton" class="sidebarButton" onclick="goShopping()"><p>Compra <br/>lo que sea</p>' +
            '</div>' +

            '<div id="sellButton" class="sidebarButton" onclick="goSell()"><p>Vende <br/>lo que sea</p>' +
            '</div>' +

        '</section>'
    );

    configureLayout();
}

/*
 **********************************************
 * configures listeners of the buttons included in the main layout.
 * opens the required localStorage variables.
 **********************************************
 */

function configureLayout(){
    var beforeLogin = document.getElementById("beforeLogin");
    var afterLogin = document.getElementById("afterLogin");
    var loginForm = document.getElementById("loginForm");
    var showLogButton = document.getElementById("showLogButton");
    var accountForm = document.getElementById("accountForm");
    var accountButton = document.getElementById("accountButton");

    showLogButton.addEventListener("mouseover", showLogin, false);
    showLogButton.addEventListener("mouseout", hideLogin, false);
    loginForm.addEventListener("mouseover", showLogin, false);
    loginForm.addEventListener("mouseout", hideLogin, false);

    accountButton.addEventListener("mouseover", showAccForm, false);
    accountButton.addEventListener("mouseout", hideAccForm, false);
    accountForm.addEventListener("mouseover", showAccForm, false);
    accountForm.addEventListener("mouseout", hideAccForm, false);

    loginForm.style.display = "none";
    accountForm.style.display = "none";

    if(localStorage.getItem("logged")==="true"){
        showHeader(true);
    }else{
        showHeader(false);
    }
}


